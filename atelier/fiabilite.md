# 💯 Fiabilité

- <a href="https://www.radiofrance.fr/franceinter/podcasts/le-code-a-change/le-code-a-change-6-5342040" target="_blank">Les dames de l'algorithme</a>, France Inter, 19/01/24

- <a href="https://scholar.harvard.edu/files/gking/files/0314policyforumff.pdf" target="_blank">The Parable of Google Flu : Traps in Big Data Analysis</a>, Science Mag, 14/03/14