<style>

   .iframe-wrapper {
  max-width: 60%;
  margin: auto; /* pour centrer le conteneur */
}

    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 🎨 Créativité

- <a href="https://www.radiofrance.fr/franceinter/podcasts/un-monde-nouveau/un-monde-nouveau-du-jeudi-28-mars-2024-9880614" target="_blank">Bigflo et Oli, un mariage controversé entre le rap et l’IA</a>, France Inter, 28/03/24

- <a href="https://youtu.be/SIyGif6p1GQ" target="_blank">Bigflo & Oli - Ça va beaucoup trop vite (Clip IA)</a>,  Bigflo et Oli, 24/03/24

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/SIyGif6p1GQ" title="Bigflo & Oli - Ça va beaucoup trop vite (Clip IA)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

- <a href="https://www.arte.tv/fr/videos/110342-003-A/le-dessous-des-images/" target="_blank">Le dessous des images - L'oeuvre et l'intelligence artificielle</a>, Arte, 10/05/23