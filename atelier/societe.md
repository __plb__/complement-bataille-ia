<style>

   .iframe-wrapper {
  max-width: 60%;
  margin: auto; /* pour centrer le conteneur */
}

    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 👥 Société

- <a href="https://youtu.be/Zjr1bqE8Bg0" traget="_blank">Le B.A.-BA de l'intelligence artificielle | Une leçon de géopolitique | ARTE</a>, Le Dessous des Cartes - ARTE, 15/03/24

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/Zjr1bqE8Bg0" title="Le B.A.-BA de l'intelligence artificielle | Une leçon de géopolitique | ARTE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

- <a href="https://www.radiofrance.fr/franceinter/podcasts/veille-sanitaire/veille-sanitaire-du-vendredi-29-mars-2024-4569994" target="_blank">Des influenceuses victimes de deepfakes sur les réseaux chinois</a>, France Inter, 29/03/24

- <a href="https://www.radiofrance.fr/franceculture/podcasts/le-biais-d-aurelie-jean/le-biais-d-aurelie-jean-chronique-du-mardi-26-mars-2024-5747798" target="_blank">IA : pourquoi les intellectuels paniquent-ils ?</a>, France Culture, 26/03/2024

- <a href="https://youtu.be/HEZogcu7x_o" target="_blank">L’IA va-t-elle perturber les élections en 2024 ?, AFP, 11/01/24

